package app

import (
	"fmt"
	"log"

	"github.com/spf13/viper"
)

type Config struct {
	Name string `mapstructure:"name"`
	Env  string `mapstructure:"env"`
}

func NewConfig() (*Config, error) {
	if !viper.InConfig("app") {
		log.Fatal("not existed config for app")
	}

	config := &Config{}
	if err := viper.UnmarshalKey("app", config); err != nil {
		return nil, err
	}

	if config.Name == "" {
		return nil, fmt.Errorf("invalid config for app: name=EMPTY")
	}
	if config.Env == "" {
		log.Printf("unexpected config for app: env=EMPTY")
		config.Env = LocalEnv
		log.Printf("fallback to config for app: env=%s", LocalEnv)
	}
	return config, nil
}

func (c *Config) GetServiceName() string {
	return fmt.Sprintf("%s-%s", c.Name, c.Env)
}
