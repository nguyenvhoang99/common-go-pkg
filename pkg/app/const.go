package app

const (
	LocalEnv       = "local"
	DevelopmentEnv = "development"
	SandboxEnv     = "sandbox"
	StagingEnv     = "staging"
	LoadtestEnv    = "loadtest"
	ProductionEnv  = "production"
)
