package app_fx

import (
	"go.uber.org/fx"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/app"
)

var ConfigModule = fx.Provide(app.NewConfig)
