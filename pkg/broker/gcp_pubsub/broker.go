package gcp_pubsub

import (
	"context"

	"cloud.google.com/go/pubsub"
	"go.uber.org/zap"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/app"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/tracer"
)

type Broker struct {
	Client              *pubsub.Client
	projectId           string
	consumerServicesMap map[string][]IConsumerService // key: subcriptionId, value: consumerServices
	consumersCancel     context.CancelFunc
	logger              logger.ILogger
	tracer              tracer.ITracer
}

type IConsumerService interface {
	SubcriptionId() string
	Handle() func(ctx context.Context, data []byte, attributes map[string]string)
}

func NewBroker(config *Config, appConfig *app.Config, consumerServices []IConsumerService,
	logger logger.ILogger, tracer tracer.ITracer) *Broker {
	broker := &Broker{
		projectId:           config.ProjectId,
		consumerServicesMap: make(map[string][]IConsumerService),
		logger:              logger,
		tracer:              tracer,
	}

	if len(consumerServices) > 0 {
		for _, consumerService := range consumerServices {
			if _, ok := broker.consumerServicesMap[consumerService.SubcriptionId()]; !ok {
				broker.consumerServicesMap[consumerService.SubcriptionId()] = make([]IConsumerService, 0)
			}
			broker.consumerServicesMap[consumerService.SubcriptionId()] = append(broker.consumerServicesMap[consumerService.SubcriptionId()], consumerService)
		}
	}

	return broker
}

func Start(ctx context.Context, self *Broker) error {
	self.logger.Info("connecting to gcp pubsub broker")
	client, err := pubsub.NewClient(ctx, self.projectId)
	if err != nil {
		self.logger.Error("failed to connect to gcp pubsub broker", zap.Error(err))
		return err
	}
	self.logger.Info("connected to gcp pubsub broker")
	self.Client = client

	if len(self.consumerServicesMap) > 0 {
		consumersCtx, consumersCancel := context.WithCancel(context.Background())
		for subcriptionId, consumerServices := range self.consumerServicesMap {
			subcription := self.Client.Subscription(subcriptionId)
			subcription.Receive(consumersCtx, func(ctx context.Context, msg *pubsub.Message) {
				for _, consumerService := range consumerServices {
					go consumerService.Handle()(consumersCtx, msg.Data, msg.Attributes)
				}
				msg.Ack()
			})
		}
		self.consumersCancel = consumersCancel
	}
	return nil
}

func Stop(ctx context.Context, self *Broker) error {
	if len(self.consumerServicesMap) > 0 {
		self.logger.Info("unsubscribing from gcp pubsub broker")
		self.consumersCancel()
		self.logger.Info("unsubscribed from gcp pubsub broker")
	}
	self.logger.Info("disconnecting from gcp pubsub broker")
	if err := self.Client.Close(); err != nil {
		self.logger.Error("failed to disconnect from gcp pubsub broker")
		return err
	}
	self.logger.Info("disconnected from gcp pubsub broker")
	return nil
}
