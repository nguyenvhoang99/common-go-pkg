package gcp_pubsub

import (
	"fmt"

	"github.com/spf13/viper"
)

type Config struct {
	ProjectId string `mapstructure:"project-id"`
}

const configKey = "gcp-pubsub";

func NewConfig() (*Config, error) {
	if !viper.InConfig(configKey) {
		return nil, nil
	}

	config := &Config{}
	err := viper.UnmarshalKey(configKey, config)
	if err != nil {
		return nil, err
	}

	if config.ProjectId == "" {
		return nil, fmt.Errorf("invalid config for gcp pubsub: project-id=EMPTY")
	}
	return config, nil
}
