package gcp_pubsub_fx

import (
	"context"

	"go.uber.org/fx"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/app"
	gcp_pubsub_broker "gitlab.com/nguyenvhoang99/common-go-pkg/pkg/broker/gcp_pubsub"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/tracer"
)

var BrokerModule = fx.Provide(gcp_pubsub_broker.NewConfig,
	fx.Annotate(
		NewBrokerForFx,
		fx.ParamTags(``, ``, ``, `group:"gcp-pubsub-consumer-services"`, ``, ``),
	))

func NewBrokerForFx(lc fx.Lifecycle, config *gcp_pubsub_broker.Config, appConfig *app.Config,
	consumerServices []gcp_pubsub_broker.IConsumerService,
	logger logger.ILogger, tracer tracer.ITracer) (*gcp_pubsub_broker.Broker, error) {
	self := gcp_pubsub_broker.NewBroker(config, appConfig, consumerServices, logger, tracer)
	if self == nil {
		return nil, nil
	}

	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			return gcp_pubsub_broker.Start(ctx, self)
		},
		OnStop: func(ctx context.Context) error {
			return gcp_pubsub_broker.Stop(ctx, self)
		},
	})
	return self, nil
}

func AsConsumerServiceForFx(consumerService any) any {
	return fx.Annotate(
		consumerService,
		fx.As(new(gcp_pubsub_broker.IConsumerService)),
		fx.ResultTags(`group:"gcp-pubsub-consumer-services"`),
	)
}
