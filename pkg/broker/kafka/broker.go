package kafka_broker

import (
	"context"
	"crypto/tls"
	"fmt"

	kafka "github.com/twmb/franz-go/pkg/kgo"
	"github.com/twmb/franz-go/pkg/sasl"
	"github.com/twmb/franz-go/pkg/sasl/scram"
	"github.com/twmb/franz-go/plugin/kotel"
	"go.uber.org/zap"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/app"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/tracer"
)

const (
	sha256Mechanism = "SHA-256"
	sha512Mechanism = "SHA-512"
)

type Broker struct {
	Client              *kafka.Client
	consumerServicesMap map[string][]IConsumerService // key: topic, value: consumerServices
	logger              logger.ILogger
}

type IConsumerService interface {
	Topic() string
	Handle() func(ctx context.Context, partition int32, offset int64, key, value []byte, headers map[string][]byte)
}

func NewBroker(config *Config, appConfig *app.Config, consumerServices []IConsumerService,
	logger logger.ILogger, tracer tracer.ITracer) (*Broker, error) {
	if config == nil {
		return nil, nil
	}

	broker := &Broker{
		consumerServicesMap: make(map[string][]IConsumerService),
		logger:              logger,
	}

	brokerConfig := make([]kafka.Opt, 0)
	brokerConfig = append(brokerConfig, kafka.SeedBrokers(config.Addrs...))
	brokerConfig = append(brokerConfig, kafka.ClientID(appConfig.GetServiceName()))

	if config.UseSSL {
		tlsConfig := &tls.Config{}
		brokerConfig = append(brokerConfig, kafka.DialTLSConfig(tlsConfig))
	}

	if config.UseAuthentication {
		auth := scram.Auth{
			User: config.Username,
			Pass: config.Password,
		}
		var mechanism sasl.Mechanism
		switch config.Mechanism {
		case sha256Mechanism:
			mechanism = auth.AsSha256Mechanism()
		case sha512Mechanism:
			mechanism = auth.AsSha512Mechanism()
		default:
			return nil, fmt.Errorf("not supported for kafka broker: mechanism=%s", config.Mechanism)
		}
		brokerConfig = append(brokerConfig, kafka.SASL(mechanism))
	}

	brokerConfig = append(brokerConfig, kafka.ConsumerGroup(config.ConsumerGroupId))
	brokerConfig = append(brokerConfig, kafka.MaxConcurrentFetches(config.ConsumeConcurrency))
	if config.ConsumeEarliestOffset {
		brokerConfig = append(brokerConfig, kafka.ConsumeResetOffset(kafka.NewOffset().AtStart()))
	} else {
		brokerConfig = append(brokerConfig, kafka.ConsumeResetOffset(kafka.NewOffset().AtEnd()))
	}

	if tracer != nil {
		brokerConfig = append(brokerConfig, kafka.WithHooks(kotel.NewTracer()))
	}

	if len(consumerServices) > 0 {
		consumeTopics := make([]string, 0)
		for _, consumerService := range consumerServices {
			if _, ok := broker.consumerServicesMap[consumerService.Topic()]; !ok {
				broker.consumerServicesMap[consumerService.Topic()] = make([]IConsumerService, 0)
				consumeTopics = append(consumeTopics, consumerService.Topic())
			}
			broker.consumerServicesMap[consumerService.Topic()] = append(broker.consumerServicesMap[consumerService.Topic()], consumerService)
		}
		brokerConfig = append(brokerConfig, kafka.ConsumeTopics(consumeTopics...))
	}

	brokerClient, err := kafka.NewClient(brokerConfig...)
	if err != nil {
		return nil, err
	}
	broker.Client = brokerClient

	return broker, nil
}

func Start(ctx context.Context, self *Broker) error {
	if len(self.consumerServicesMap) > 0 {
		go func() {
			for {
				bgCtx := context.Background()
				fetches := self.Client.PollFetches(bgCtx)
				if fetches.IsClientClosed() {
					self.logger.InfoCtx(bgCtx, "kafka broker has been disconnected")
					return
				}
				if errs := fetches.Errors(); len(errs) > 0 {
					fetches.EachError(func(topic string, partition int32, err error) {
						self.logger.ErrorCtx(bgCtx, "error when fetch from kafka broker", zap.String("topic", topic),
							zap.Int32("partition", partition), zap.Error(err))
					})
					continue
				}
				fetches.EachTopic(func(fetchTopic kafka.FetchTopic) {
					for _, consumerService := range self.consumerServicesMap[fetchTopic.Topic] {
						fetchTopic.EachRecord(func(record *kafka.Record) {
							headers := make(map[string][]byte)
							for _, recordHeader := range record.Headers {
								headers[recordHeader.Key] = recordHeader.Value
							}
							go consumerService.Handle()(record.Context, record.Partition, record.Offset, record.Key, record.Value, headers)
						})
					}
				})
			}
		}()
	}
	return nil
}

func Stop(ctx context.Context, self *Broker) error {
	self.logger.Info("disconnecting from kafka broker")
	self.Client.Close()
	self.logger.Info("disconnected from kafka broker")
	return nil
}
