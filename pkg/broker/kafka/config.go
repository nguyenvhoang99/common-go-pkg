package kafka_broker

import (
	"fmt"
	"log"

	"github.com/spf13/viper"
)

type Config struct {
	Addrs                 []string `mapstructure:"addrs"`
	UseSSL                bool     `mapstructure:"use-ssl"`
	UseAuthentication     bool     `mapstructure:"use-authentication"`
	Username              string   `mapstructure:"username"`
	Password              string   `mapstructure:"password"`
	Mechanism             string   `mapstructure:"mechanism"`
	ConnectTimeoutInSec   int      `mapstructure:"connect-timeout-in-sec"`
	ConsumerGroupId       string   `mapstructure:"consumer-group-id"`
	ConsumeConcurrency    int      `mapstructure:"consume-concurrency"`
	ConsumeEarliestOffset bool     `mapstructure:"consume-earliest-offset"`
}

func NewConfig() (*Config, error) {
	if !viper.InConfig("kafka-broker") {
		return nil, nil
	}

	config := &Config{}
	err := viper.UnmarshalKey("kafka-broker", config)
	if err != nil {
		return nil, err
	}

	if len(config.Addrs) == 0 {
		return nil, fmt.Errorf("invalid config for kafka broker: addrs=%v", config.Addrs)
	}
	if config.UseAuthentication {
		if config.Username == "" {
			return nil, fmt.Errorf("invalid config for kafka broker: username=EMPTY")
		}
		if config.Password == "" {
			return nil, fmt.Errorf("invalid config for kafka broker: password=EMPTY")
		}
		if config.Mechanism == "" {
			log.Printf("unexpected config for kafka broker: mechanism=EMPTY")
			config.Mechanism = sha256Mechanism
			log.Printf("fallback to config for kafka broker: mechanism=%s", sha256Mechanism)
		}
	}
	if config.ConnectTimeoutInSec <= 0 {
		return nil, fmt.Errorf("invalid config for kafka broker: connect-timeout-in-sec=%d", config.ConnectTimeoutInSec)
	}
	if config.ConsumerGroupId == "" {
		return nil, fmt.Errorf("invalid config for kafka broker: consumer-group-id=EMPTY")
	}
	if config.ConsumeConcurrency <= 0 {
		return nil, fmt.Errorf("invalid config for kafka broker: consume-concurrency=%d", config.ConsumeConcurrency)
	}
	return config, nil
}
