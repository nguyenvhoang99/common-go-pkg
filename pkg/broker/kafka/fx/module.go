package kafka_broker_fx

import (
	"context"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/tracer"

	"go.uber.org/fx"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/app"
	kafka_broker "gitlab.com/nguyenvhoang99/common-go-pkg/pkg/broker/kafka"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
)

var BrokerModule = fx.Provide(kafka_broker.NewConfig,
	fx.Annotate(
		NewBrokerForFx,
		fx.ParamTags(``, ``, ``, `group:"kafka-consumer-services"`, ``),
	))

func NewBrokerForFx(lc fx.Lifecycle, config *kafka_broker.Config, appConfig *app.Config,
	consumerServices []kafka_broker.IConsumerService, logger logger.ILogger, tracer tracer.ITracer) (*kafka_broker.Broker, error) {
	self, err := kafka_broker.NewBroker(config, appConfig, consumerServices, logger, tracer)
	if err != nil {
		return nil, err
	}
	if self == nil {
		return nil, nil
	}

	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			return kafka_broker.Start(ctx, self)
		},
		OnStop: func(ctx context.Context) error {
			return kafka_broker.Stop(ctx, self)
		},
	})
	return self, nil
}

func AsConsumerServiceForFx(consumerService any) any {
	return fx.Annotate(
		consumerService,
		fx.As(new(kafka_broker.IConsumerService)),
		fx.ResultTags(`group:"kafka-consumer-services"`),
	)
}
