package redis_broker

import (
	"context"
	"crypto/tls"
	"time"

	"github.com/redis/go-redis/extra/redisotel/v9"
	"github.com/redis/go-redis/v9"
	"go.uber.org/zap"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/app"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/tracer"
)

type Broker struct {
	Client              redis.UniversalClient
	config              *redis.UniversalOptions
	consumerChannelsMap map[string]*redis.PubSub      // key: channel, value: consumerSub
	consumerServicesMap map[string][]IConsumerService // key: channel, value: consumerServices
	logger              logger.ILogger
	withTracing         bool
}

type IConsumerService interface {
	Channel() string
	Handle() func(ctx context.Context, payload string)
}

func NewBroker(config *Config, appConfig *app.Config, consumerServices []IConsumerService,
	logger logger.ILogger, tracer tracer.ITracer) *Broker {
	if config == nil {
		return nil
	}

	broker := &Broker{
		config: &redis.UniversalOptions{
			Addrs:        config.Addrs,
			DialTimeout:  time.Duration(config.ConnectTimeoutInSec) * time.Second,
			ReadTimeout:  time.Duration(config.ReadTimeoutInSec) * time.Second,
			WriteTimeout: time.Duration(config.WriteTimeoutInSec) * time.Second,
			MinIdleConns: int(config.MinPoolSize),
			MaxIdleConns: int(config.MaxPoolSize),
			ClientName:   appConfig.GetServiceName(),
		},
		consumerChannelsMap: make(map[string]*redis.PubSub),
		consumerServicesMap: make(map[string][]IConsumerService),
		logger:              logger,
		withTracing:         tracer != nil,
	}

	tlsConfig := &tls.Config{}
	if !config.UseSSL {
		tlsConfig = nil
	}
	broker.config.TLSConfig = tlsConfig

	if config.UseAuthentication {
		broker.config.Username = config.Username
		broker.config.Password = config.Password
	}

	if len(consumerServices) > 0 {
		for _, consumerService := range consumerServices {
			if _, ok := broker.consumerServicesMap[consumerService.Channel()]; !ok {
				broker.consumerServicesMap[consumerService.Channel()] = make([]IConsumerService, 0)
			}
			broker.consumerServicesMap[consumerService.Channel()] = append(broker.consumerServicesMap[consumerService.Channel()], consumerService)
		}
	}

	return broker
}

func Start(ctx context.Context, self *Broker) error {
	self.logger.Info("connecting to redis broker")
	client := redis.NewUniversalClient(self.config)
	if err := client.Ping(ctx).Err(); err != nil {
		self.logger.Error("failed to connect to redis broker", zap.Error(err))
		return err
	}
	if self.withTracing {
		if err := redisotel.InstrumentTracing(client); err != nil {
			self.logger.Error("failed to instrument tracing for redis broker", zap.Error(err))
			return err
		}
	}
	self.Client = client
	self.logger.Info("connected to redis broker")

	if len(self.consumerServicesMap) > 0 {
		for channel, consumerServices := range self.consumerServicesMap {
			bgCtx := context.Background()
			self.logger.Info("subscribing to redis broker", zap.String("channel", channel))
			consumerSub := self.Client.Subscribe(bgCtx, channel)
			self.consumerChannelsMap[channel] = consumerSub
			self.logger.Info("subscribed to redis broker", zap.String("channel", channel))

			go func(consumerSub *redis.PubSub, consumerServices []IConsumerService) {
				for msg := range consumerSub.Channel() {
					for _, consumerService := range consumerServices {
						go consumerService.Handle()(context.Background(), msg.Payload)
					}
				}
			}(consumerSub, consumerServices)
		}
	}
	return nil
}

func Stop(ctx context.Context, self *Broker) error {
	for channel, consumerSub := range self.consumerChannelsMap {
		self.logger.Info("unsubscribing from redis broker", zap.String("channel", channel))
		if err := consumerSub.Close(); err != nil {
			self.logger.Error("failed to unsubscribe from redis broker", zap.String("channel", channel), zap.Error(err))
			continue
		}
		self.logger.Info("unsubscribed from redis broker", zap.String("channel", channel))
	}
	self.logger.Info("disconnecting from redis broker")
	if err := self.Client.Close(); err != nil {
		self.logger.Error("failed to disconnect from redis broker")
		return err
	}
	self.logger.Info("disconnected from redis broker")
	return nil
}
