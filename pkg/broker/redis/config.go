package redis_broker

import (
	"fmt"

	"github.com/spf13/viper"
)

type Config struct {
	Addrs               []string `mapstructure:"addrs"`
	UseSSL              bool     `mapstructure:"use-ssl"`
	UseAuthentication   bool     `mapstructure:"use-authentication"`
	Username            string   `mapstructure:"username"`
	Password            string   `mapstructure:"password"`
	ConnectTimeoutInSec uint32   `mapstructure:"connect-timeout-in-sec"`
	ReadTimeoutInSec    uint32   `mapstructure:"read-timeout-in-sec"`
	WriteTimeoutInSec   uint32   `mapstructure:"write-timeout-in-sec"`
	MinPoolSize         uint16   `mapstructure:"min-pool-size"`
	MaxPoolSize         uint16   `mapstructure:"max-pool-size"`
}

func NewConfig() (*Config, error) {
	if !viper.InConfig("redis-broker") {
		return nil, nil
	}

	config := &Config{}
	if err := viper.UnmarshalKey("redis-broker", config); err != nil {
		return nil, err
	}

	if len(config.Addrs) == 0 {
		return nil, fmt.Errorf("invalid config for redis broker: addrs=%v", config.Addrs)
	}
	if config.UseAuthentication {
		if config.Username == "" {
			return nil, fmt.Errorf("invalid config for redis broker: username=EMPTY")
		}
		if config.Password == "" {
			return nil, fmt.Errorf("invalid config for redis broker: password=EMPTY")
		}
	}
	if config.ConnectTimeoutInSec <= 0 {
		return nil, fmt.Errorf("invalid config for redis broker: connect-timeout-in-sec=%d", config.ConnectTimeoutInSec)
	}
	if config.ReadTimeoutInSec <= 0 {
		return nil, fmt.Errorf("invalid config for redis broker: read-timeout-in-sec=%d", config.ReadTimeoutInSec)
	}
	if config.WriteTimeoutInSec <= 0 {
		return nil, fmt.Errorf("invalid config for redis broker: write-timeout-in-sec=%d", config.WriteTimeoutInSec)
	}
	if config.MinPoolSize <= 0 {
		return nil, fmt.Errorf("invalid config for redis broker: min-pool-size=%d", config.MinPoolSize)
	}
	if config.MaxPoolSize <= 0 {
		return nil, fmt.Errorf("invalid config for redis broker: max-pool-size=%d", config.MaxPoolSize)
	}
	if config.MinPoolSize > config.MaxPoolSize {
		return nil, fmt.Errorf("invalid config for redis broker: min-pool-size=%d | max-pool-size=%d", config.MinPoolSize, config.MaxPoolSize)
	}
	return config, nil
}
