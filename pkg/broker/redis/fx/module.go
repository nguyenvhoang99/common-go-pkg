package redis_broker_fx

import (
	"context"

	"go.uber.org/fx"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/app"
	redis_broker "gitlab.com/nguyenvhoang99/common-go-pkg/pkg/broker/redis"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/tracer"
)

var BrokerModule = fx.Provide(redis_broker.NewConfig,
	fx.Annotate(
		NewBrokerForFx,
		fx.ParamTags(``, ``, ``, `group:"redis-consumer-services"`, ``),
	))

func NewBrokerForFx(lc fx.Lifecycle, config *redis_broker.Config, appConfig *app.Config,
	consumerServices []redis_broker.IConsumerService,
	logger logger.ILogger, tracer tracer.ITracer) (*redis_broker.Broker, error) {
	self := redis_broker.NewBroker(config, appConfig, consumerServices, logger, tracer)
	if self == nil {
		return nil, nil
	}

	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			return redis_broker.Start(ctx, self)
		},
		OnStop: func(ctx context.Context) error {
			return redis_broker.Stop(ctx, self)
		},
	})
	return self, nil
}

func AsConsumerServiceForFx(consumerService any) any {
	return fx.Annotate(
		consumerService,
		fx.As(new(redis_broker.IConsumerService)),
		fx.ResultTags(`group:"redis-consumer-services"`),
	)
}
