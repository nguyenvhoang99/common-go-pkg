package cassandra_database

import (
	"fmt"

	"github.com/spf13/viper"
)

type Config struct {
	Hosts               []string `mapstructure:"hosts"`
	UseSSL              bool     `mapstructure:"use-ssl"`
	UseAuthentication   bool     `mapstructure:"use-authentication"`
	Username            string   `mapstructure:"username"`
	Password            string   `mapstructure:"password"`
	ConnectTimeoutInSec uint32   `mapstructure:"connect-timeout-in-sec"`
	WriteTimeoutInSec   uint32   `mapstructure:"write-timeout-in-sec"`
	PoolSize            uint16   `mapstructure:"pool-size"`
	Keyspace            string   `mapstructure:"keyspace"`
}

func NewConfig() (*Config, error) {
	if !viper.InConfig("cassandra-database") {
		return nil, nil
	}

	config := &Config{}
	if err := viper.UnmarshalKey("cassandra-database", config); err != nil {
		return nil, err
	}

	if len(config.Hosts) == 0 {
		return nil, fmt.Errorf("invalid config for cassandra database: hosts=%v", config.Hosts)
	}
	if config.UseAuthentication {
		if config.Username == "" {
			return nil, fmt.Errorf("invalid config for cassandra database: username=EMPTY")
		}
		if config.Password == "" {
			return nil, fmt.Errorf("invalid config for cassandra database: password=EMPTY")
		}
	}
	if config.ConnectTimeoutInSec <= 0 {
		return nil, fmt.Errorf("invalid config for cassandra database: connect-timeout-in-sec=%d", config.ConnectTimeoutInSec)
	}
	if config.WriteTimeoutInSec <= 0 {
		return nil, fmt.Errorf("invalid config for cassandra database: write-timeout-in-sec=%d", config.WriteTimeoutInSec)
	}
	if config.PoolSize <= 0 {
		return nil, fmt.Errorf("invalid config for cassandra database: pool-size=%d", config.PoolSize)
	}
	if config.Keyspace == "" {
		return nil, fmt.Errorf("invalid config for cassandra database: keyspace=EMPTY")
	}
	return config, nil
}
