package cassandra_database

import (
	"context"
	"crypto/tls"
	"time"

	"github.com/gocql/gocql"
	"go.opentelemetry.io/contrib/instrumentation/github.com/gocql/gocql/otelgocql"
	"go.uber.org/zap"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/tracer"
)

type Database struct {
	Client      *gocql.Session
	config      *gocql.ClusterConfig
	logger      logger.ILogger
	withTracing bool
}

func NewDatabase(config *Config, logger logger.ILogger, tracer tracer.ITracer) *Database {
	if config == nil {
		return nil
	}

	database := &Database{
		config: &gocql.ClusterConfig{
			Hosts:          config.Hosts,
			ConnectTimeout: time.Duration(config.ConnectTimeoutInSec) * time.Second,
			WriteTimeout:   time.Duration(config.WriteTimeoutInSec) * time.Second,
			NumConns:       int(config.PoolSize),
			Keyspace:       config.Keyspace,
		},
		logger:      logger,
		withTracing: tracer != nil,
	}

	tlsConfig := &tls.Config{}
	if !config.UseSSL {
		tlsConfig = nil
	}
	database.config.SslOpts.Config = tlsConfig

	if config.UseAuthentication {
		database.config.Authenticator = gocql.PasswordAuthenticator{
			Username: config.Username,
			Password: config.Password,
		}
	}

	return database
}

func Connect(ctx context.Context, self *Database) error {
	self.logger.Info("connecting to cassandra database")
	var client *gocql.Session
	var err error
	if self.withTracing {
		client, err = otelgocql.NewSessionWithTracing(ctx, self.config)
	} else {
		client, err = self.config.CreateSession()
	}
	if err != nil {
		self.logger.Error("failed to connect to cassandra database", zap.Error(err))
		return err
	}
	self.Client = client
	self.logger.Info("connected to cassandra database")
	return nil
}

func Disconnect(ctx context.Context, self *Database) error {
	self.logger.Info("disconnecting from cassandra database")
	self.Client.Close()
	self.logger.Info("disconnected from cassandra database")
	return nil
}
