package cassandra_database_fx

import (
	"context"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/tracer"

	"go.uber.org/fx"

	cassandra_database "gitlab.com/nguyenvhoang99/common-go-pkg/pkg/database/cassandra"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
)

var DatabaseModule = fx.Provide(cassandra_database.NewConfig, NewDatabaseForFx)

func NewDatabaseForFx(lc fx.Lifecycle, config *cassandra_database.Config,
	logger logger.ILogger, tracer tracer.ITracer) *cassandra_database.Database {
	self := cassandra_database.NewDatabase(config, logger, tracer)
	if self == nil {
		return nil
	}

	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			return cassandra_database.Connect(ctx, self)
		},
		OnStop: func(ctx context.Context) error {
			return cassandra_database.Disconnect(ctx, self)
		},
	})
	return self
}
