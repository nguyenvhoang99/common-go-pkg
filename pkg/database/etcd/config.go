package etcd_database

import (
	"fmt"

	"github.com/spf13/viper"
)

type Config struct {
	Hosts               []string `mapstructure:"hosts"`
	UseSSL              bool     `mapstructure:"use-ssl"`
	UseAuthentication   bool     `mapstructure:"use-authentication"`
	Username            string   `mapstructure:"username"`
	Password            string   `mapstructure:"password"`
	ConnectTimeoutInSec uint32   `mapstructure:"connect-timeout-in-sec"`
}

func NewConfig() (*Config, error) {
	if !viper.InConfig("etcd-database") {
		return nil, nil
	}

	config := &Config{}
	if err := viper.UnmarshalKey("etcd-database", config); err != nil {
		return nil, err
	}

	if len(config.Hosts) == 0 {
		return nil, fmt.Errorf("invalid config for etcd database: hosts=%v", config.Hosts)
	}
	if config.UseAuthentication {
		if config.Username == "" {
			return nil, fmt.Errorf("invalid config for etcd database: username=EMPTY")
		}
		if config.Password == "" {
			return nil, fmt.Errorf("invalid config for etcd database: password=EMPTY")
		}
	}
	if config.ConnectTimeoutInSec <= 0 {
		return nil, fmt.Errorf("invalid config for etcd database: connect-timeout-in-sec=%d", config.ConnectTimeoutInSec)
	}
	return config, nil
}
