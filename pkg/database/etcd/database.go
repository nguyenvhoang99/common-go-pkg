package etcd_database

import (
	"context"
	"crypto/tls"
	"time"

	etcd_client "go.etcd.io/etcd/client/v3"
	"go.uber.org/zap"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
)

type Database struct {
	Client *etcd_client.Client
	config etcd_client.Config
	logger logger.ILogger
}

func NewDatabase(config *Config, logger logger.ILogger) *Database {
	if config == nil {
		return nil
	}

	database := &Database{
		config: etcd_client.Config{
			Endpoints:   config.Hosts,
			DialTimeout: time.Duration(config.ConnectTimeoutInSec) * time.Second,
		},
		logger: logger,
	}

	tlsConfig := &tls.Config{}
	if !config.UseSSL {
		tlsConfig = nil
	}
	database.config.TLS = tlsConfig

	if config.UseAuthentication {
		database.config.Username = config.Username
		database.config.Password = config.Password
	}

	return database
}

func Connect(ctx context.Context, self *Database) error {
	self.logger.Info("connecting to etcd database")
	client, err := etcd_client.New(self.config)
	if err != nil {
		self.logger.Error("failed to connect to etcd database", zap.Error(err))
		return err
	}
	self.Client = client
	self.logger.Info("connected to etcd database")
	return nil
}

func Disconnect(ctx context.Context, self *Database) error {
	self.logger.Info("disconnecting from etcd database")
	if err := self.Client.Close(); err != nil {
		self.logger.Error("failed to disconnect from etcd database", zap.Error(err))
		return err
	}
	self.logger.Info("disconnected from etcd database")
	return nil
}
