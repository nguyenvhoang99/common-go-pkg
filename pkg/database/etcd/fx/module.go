package etcd_database_fx

import (
	"context"

	"go.uber.org/fx"

	etcd_database "gitlab.com/nguyenvhoang99/common-go-pkg/pkg/database/etcd"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
)

var DatabaseModule = fx.Provide(etcd_database.NewConfig, NewDatabaseForFx)

func NewDatabaseForFx(lc fx.Lifecycle, config *etcd_database.Config, logger logger.ILogger) *etcd_database.Database {
	self := etcd_database.NewDatabase(config, logger)
	if self == nil {
		return nil
	}

	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			return etcd_database.Connect(ctx, self)
		},
		OnStop: func(ctx context.Context) error {
			return etcd_database.Disconnect(ctx, self)
		},
	})
	return self
}
