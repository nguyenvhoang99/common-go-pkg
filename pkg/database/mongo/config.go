package mongo_database

import (
	"fmt"

	"github.com/spf13/viper"
)

type Config struct {
	URI                   string `mapstructure:"uri"`
	UseSSL                bool   `mapstructure:"use-ssl"`
	UseAuthentication     bool   `mapstructure:"use-authentication"`
	Username              string `mapstructure:"username"`
	Password              string `mapstructure:"password"`
	ConnectTimeoutInSec   uint32 `mapstructure:"connect-timeout-in-sec"`
	OperationTimeoutInSec uint32 `mapstructure:"operation-timeout-in-sec"`
	MinPoolSize           uint16 `mapstructure:"min-pool-size"`
	MaxPoolSize           uint16 `mapstructure:"max-pool-size"`
	DatabaseName          string `mapstructure:"database-name"`
}

func NewConfig() (*Config, error) {
	if !viper.InConfig("mongo-database") {
		return nil, nil
	}

	config := &Config{}
	if err := viper.UnmarshalKey("mongo-database", config); err != nil {
		return nil, err
	}

	if config.URI == "" {
		return nil, fmt.Errorf("invalid config for mongo database: uri=EMPTY")
	}
	if config.UseAuthentication {
		if config.Username == "" {
			return nil, fmt.Errorf("invalid config for mongo database: username=EMPTY")
		}
		if config.Password == "" {
			return nil, fmt.Errorf("invalid config for mongo database: password=EMPTY")
		}
	}
	if config.ConnectTimeoutInSec <= 0 {
		return nil, fmt.Errorf("invalid config for mongo database: connect-timeout-in-sec=%d", config.ConnectTimeoutInSec)
	}
	if config.OperationTimeoutInSec <= 0 {
		return nil, fmt.Errorf("invalid config for mongo database: operation-timeout-in-sec=%d", config.OperationTimeoutInSec)
	}
	if config.MinPoolSize <= 0 {
		return nil, fmt.Errorf("invalid config for mongo database: min-pool-size=%d", config.MinPoolSize)
	}
	if config.MaxPoolSize <= 0 {
		return nil, fmt.Errorf("invalid config for mongo database: max-pool-size=%d", config.MaxPoolSize)
	}
	if config.MinPoolSize > config.MaxPoolSize {
		return nil, fmt.Errorf("invalid config for mongo database: min-pool-size=%d | max-pool-size=%d", config.MinPoolSize, config.MaxPoolSize)
	}
	if config.DatabaseName == "" {
		return nil, fmt.Errorf("invalid config for mongo database: database-name=EMPTY")
	}
	return config, nil
}
