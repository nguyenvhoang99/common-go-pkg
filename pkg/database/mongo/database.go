package mongo_database

import (
	"context"
	"crypto/tls"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.opentelemetry.io/contrib/instrumentation/go.mongodb.org/mongo-driver/mongo/otelmongo"
	"go.uber.org/zap"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/app"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/tracer"
)

type Database struct {
	Client *mongo.Database
	config *options.ClientOptions
	name   string
	logger logger.ILogger
}

func NewDatabase(config *Config, appConfig *app.Config,
	logger logger.ILogger, tracer tracer.ITracer) *Database {
	if config == nil {
		return nil
	}

	database := &Database{
		config: options.Client().
			ApplyURI(config.URI).
			SetConnectTimeout(time.Duration(config.ConnectTimeoutInSec) * time.Second).
			SetTimeout(time.Duration(config.OperationTimeoutInSec) * time.Second).
			SetMinPoolSize(uint64(config.MinPoolSize)).
			SetMaxPoolSize(uint64(config.MaxPoolSize)).
			SetAppName(appConfig.GetServiceName()),
		name:   config.DatabaseName,
		logger: logger,
	}

	tlsConfig := &tls.Config{}
	if !config.UseSSL {
		tlsConfig = nil
	}
	database.config.TLSConfig = tlsConfig

	if config.UseAuthentication {
		database.config.Auth = &options.Credential{
			Username: config.Username,
			Password: config.Password,
		}
	}

	if tracer != nil {
		database.config.Monitor = otelmongo.NewMonitor()
	}

	return database
}

func Connect(ctx context.Context, self *Database) error {
	self.logger.Info("connecting to mongo database")
	client, err := mongo.Connect(ctx, self.config)
	if err != nil {
		self.logger.Error("failed to connect to mongo database", zap.Error(err))
		return err
	}
	self.Client = client.Database(self.name)
	if err = self.Client.RunCommand(ctx, bson.D{{"ping", 1}}).Err(); err != nil {
		self.logger.Error("failed to ping mongo database", zap.Error(err))
		return err
	}
	self.logger.Info("connected to mongo database")
	return nil
}

func Disconnect(ctx context.Context, self *Database) error {
	self.logger.Info("disconnecting from mongo database")
	if err := self.Client.Client().Disconnect(ctx); err != nil {
		self.logger.Error("failed to disconnect from mongo database", zap.Error(err))
		return err
	}
	self.logger.Info("disconnected from mongo database")
	return nil
}
