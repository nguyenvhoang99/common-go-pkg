package mongo_database_fx

import (
	"context"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/tracer"

	"go.uber.org/fx"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/app"
	mongo_database "gitlab.com/nguyenvhoang99/common-go-pkg/pkg/database/mongo"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
)

var DatabaseModule = fx.Provide(mongo_database.NewConfig, NewDatabaseForFx)

func NewDatabaseForFx(lc fx.Lifecycle, config *mongo_database.Config, appConfig *app.Config,
	logger logger.ILogger, tracer tracer.ITracer) *mongo_database.Database {
	self := mongo_database.NewDatabase(config, appConfig, logger, tracer)
	if self == nil {
		return nil
	}

	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			return mongo_database.Connect(ctx, self)
		},
		OnStop: func(ctx context.Context) error {
			return mongo_database.Disconnect(ctx, self)
		},
	})
	return self
}
