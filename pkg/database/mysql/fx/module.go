package mysql_database_fx

import (
	"context"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/tracer"

	"go.uber.org/fx"

	mysql_database "gitlab.com/nguyenvhoang99/common-go-pkg/pkg/database/mysql"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
)

var DatabaseModule = fx.Provide(mysql_database.NewConfig, NewDatabaseForFx)

func NewDatabaseForFx(lc fx.Lifecycle, config *mysql_database.Config,
	logger logger.ILogger, tracer tracer.ITracer) *mysql_database.Database {
	self := mysql_database.NewDatabase(config, logger, tracer)
	if self == nil {
		return nil
	}

	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			return mysql_database.Connect(ctx, self)
		},
		OnStop: func(ctx context.Context) error {
			return mysql_database.Disconnect(ctx, self)
		},
	})
	return self
}
