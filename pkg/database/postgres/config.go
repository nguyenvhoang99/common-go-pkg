package postgres_database

import (
	"fmt"

	"github.com/spf13/viper"
)

type Config struct {
	URI         string `mapstructure:"uri"` // [user[:password]@][net[(addr)]]/dbname[?param1=value1&paramN=valueN]
	MinPoolSize uint16 `mapstructure:"min-pool-size"`
	MaxPoolSize uint16 `mapstructure:"max-pool-size"`
}

func NewConfig() (*Config, error) {
	if !viper.InConfig("postgres-database") {
		return nil, nil
	}

	config := &Config{}
	if err := viper.UnmarshalKey("postgres-database", config); err != nil {
		return nil, err
	}

	if config.URI == "" {
		return nil, fmt.Errorf("invalid config for postgres database: uri=EMPTY")
	}
	if config.MinPoolSize <= 0 {
		return nil, fmt.Errorf("invalid config for postgres database: min-pool-size=%d", config.MinPoolSize)
	}
	if config.MaxPoolSize <= 0 {
		return nil, fmt.Errorf("invalid config for postgres database: max-pool-size=%d", config.MaxPoolSize)
	}
	if config.MinPoolSize > config.MaxPoolSize {
		return nil, fmt.Errorf("invalid config for postgres database: min-pool-size=%d | max-pool-size=%d", config.MinPoolSize, config.MaxPoolSize)
	}
	return config, nil
}
