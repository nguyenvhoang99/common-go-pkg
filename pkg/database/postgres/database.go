package postgres_database

import (
	"context"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/uptrace/opentelemetry-go-extra/otelsql"
	"github.com/uptrace/opentelemetry-go-extra/otelsqlx"
	semconv "go.opentelemetry.io/otel/semconv/v1.16.0"
	"go.uber.org/zap"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/tracer"
)

type Database struct {
	Client      *sqlx.DB
	config      *Config
	logger      logger.ILogger
	withTracing bool
}

func NewDatabase(config *Config, logger logger.ILogger, tracer tracer.ITracer) *Database {
	if config == nil {
		return nil
	}
	return &Database{
		config:      config,
		logger:      logger,
		withTracing: tracer != nil,
	}
}

func Connect(ctx context.Context, self *Database) error {
	self.logger.Info("connecting to postgres database")
	var client *sqlx.DB
	var err error
	if self.withTracing {
		client, err = otelsqlx.ConnectContext(ctx, "postgres", self.config.URI,
			otelsql.WithAttributes(semconv.DBSystemPostgreSQL))
	} else {
		client, err = sqlx.ConnectContext(ctx, "postgres", self.config.URI)
	}
	if err != nil {
		self.logger.Error("failed to connect to postgres database", zap.Error(err))
		return err
	}
	client.SetMaxIdleConns(int(self.config.MinPoolSize))
	client.SetMaxOpenConns(int(self.config.MaxPoolSize))
	self.Client = client
	self.logger.Info("connected to postgres database")
	return nil
}

func Disconnect(ctx context.Context, self *Database) error {
	self.logger.Info("disconnecting from postgres database")
	if err := self.Client.Close(); err != nil {
		self.logger.Error("failed to disconnect from postgres database", zap.Error(err))
		return err
	}
	self.logger.Info("disconnected from postgres database")
	return nil
}
