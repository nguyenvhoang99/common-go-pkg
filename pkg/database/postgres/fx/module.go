package postgres_database_fx

import (
	"context"

	"go.uber.org/fx"

	postgres_database "gitlab.com/nguyenvhoang99/common-go-pkg/pkg/database/postgres"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/tracer"
)

var DatabaseModule = fx.Provide(postgres_database.NewConfig, NewDatabaseForFx)

func NewDatabaseForFx(lc fx.Lifecycle, config *postgres_database.Config,
	logger logger.ILogger, tracer tracer.ITracer) *postgres_database.Database {
	self := postgres_database.NewDatabase(config, logger, tracer)
	if self == nil {
		return nil
	}

	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			return postgres_database.Connect(ctx, self)
		},
		OnStop: func(ctx context.Context) error {
			return postgres_database.Disconnect(ctx, self)
		},
	})
	return self
}
