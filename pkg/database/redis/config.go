package redis_database

import (
	"fmt"

	"github.com/spf13/viper"
)

type Config struct {
	Hosts               []string `mapstructure:"hosts"`
	UseSSL              bool     `mapstructure:"use-ssl"`
	UseAuthentication   bool     `mapstructure:"use-authentication"`
	Username            string   `mapstructure:"username"`
	Password            string   `mapstructure:"password"`
	ConnectTimeoutInSec uint32   `mapstructure:"connect-timeout-in-sec"`
	ReadTimeoutInSec    uint32   `mapstructure:"read-timeout-in-sec"`
	WriteTimeoutInSec   uint32   `mapstructure:"write-timeout-in-sec"`
	MinPoolSize         uint16   `mapstructure:"min-pool-size"`
	MaxPoolSize         uint16   `mapstructure:"max-pool-size"`
}

func NewConfig() (*Config, error) {
	if !viper.InConfig("redis-database") {
		return nil, nil
	}

	config := &Config{}
	if err := viper.UnmarshalKey("redis-database", config); err != nil {
		return nil, err
	}

	if len(config.Hosts) == 0 {
		return nil, fmt.Errorf("invalid config for redis database: hosts=%v", config.Hosts)
	}
	if config.UseAuthentication {
		if config.Username == "" {
			return nil, fmt.Errorf("invalid config for redis database: username=EMPTY")
		}
		if config.Password == "" {
			return nil, fmt.Errorf("invalid config for redis database: password=EMPTY")
		}
	}
	if config.ConnectTimeoutInSec <= 0 {
		return nil, fmt.Errorf("invalid config for redis database: connect-timeout-in-sec=%d", config.ConnectTimeoutInSec)
	}
	if config.ReadTimeoutInSec <= 0 {
		return nil, fmt.Errorf("invalid config for redis database: read-timeout-in-sec=%d", config.ReadTimeoutInSec)
	}
	if config.WriteTimeoutInSec <= 0 {
		return nil, fmt.Errorf("invalid config for redis database: write-timeout-in-sec=%d", config.WriteTimeoutInSec)
	}
	if config.MinPoolSize <= 0 {
		return nil, fmt.Errorf("invalid config for redis database: min-pool-size=%d", config.MinPoolSize)
	}
	if config.MaxPoolSize <= 0 {
		return nil, fmt.Errorf("invalid config for redis database: max-pool-size=%d", config.MaxPoolSize)
	}
	if config.MinPoolSize > config.MaxPoolSize {
		return nil, fmt.Errorf("invalid config for redis database: min-pool-size=%d | max-pool-size=%d", config.MinPoolSize, config.MaxPoolSize)
	}
	return config, nil
}
