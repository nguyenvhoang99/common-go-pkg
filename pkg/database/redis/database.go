package redis_database

import (
	"context"
	"crypto/tls"
	"time"

	"github.com/redis/go-redis/extra/redisotel/v9"
	"github.com/redis/go-redis/v9"
	"go.uber.org/zap"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/app"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/tracer"
)

type Database struct {
	Client      redis.UniversalClient
	config      *redis.UniversalOptions
	logger      logger.ILogger
	withTracing bool
}

func NewDatabase(config *Config, appConfig *app.Config, logger logger.ILogger, tracer tracer.ITracer) *Database {
	if config == nil {
		return nil
	}

	database := &Database{
		config: &redis.UniversalOptions{
			Addrs:        config.Hosts,
			DialTimeout:  time.Duration(config.ConnectTimeoutInSec) * time.Second,
			ReadTimeout:  time.Duration(config.ReadTimeoutInSec) * time.Second,
			WriteTimeout: time.Duration(config.WriteTimeoutInSec) * time.Second,
			MinIdleConns: int(config.MinPoolSize),
			MaxIdleConns: int(config.MaxPoolSize),
			ClientName:   appConfig.GetServiceName(),
		},
		logger:      logger,
		withTracing: tracer != nil,
	}

	tlsConfig := &tls.Config{}
	if !config.UseSSL {
		tlsConfig = nil
	}
	database.config.TLSConfig = tlsConfig

	if config.UseAuthentication {
		database.config.Username = config.Username
		database.config.Password = config.Password
	}

	return database
}

func Connect(ctx context.Context, self *Database) error {
	self.logger.Info("connecting to redis database")
	client := redis.NewUniversalClient(self.config)
	if err := client.Ping(ctx).Err(); err != nil {
		self.logger.Error("failed to connect to redis database", zap.Error(err))
		return err
	}
	if self.withTracing {
		if err := redisotel.InstrumentTracing(client); err != nil {
			self.logger.Error("failed to instrument tracing for redis database", zap.Error(err))
			return err
		}
	}
	self.Client = client
	self.logger.Info("connected to redis database")
	return nil
}

func Disconnect(ctx context.Context, self *Database) error {
	self.logger.Info("disconnecting from redis database")
	if err := self.Client.Close(); err != nil {
		self.logger.Error("failed to disconnect from redis database", zap.Error(err))
		return err
	}
	self.logger.Info("disconnected from redis database")
	return nil
}
