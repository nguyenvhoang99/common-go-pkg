package redis_database_fx

import (
	"context"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/tracer"

	"go.uber.org/fx"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/app"
	redis_database "gitlab.com/nguyenvhoang99/common-go-pkg/pkg/database/redis"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
)

var DatabaseModule = fx.Provide(redis_database.NewConfig, NewDatabaseForFx)

func NewDatabaseForFx(lc fx.Lifecycle, config *redis_database.Config, appConfig *app.Config,
	logger logger.ILogger, tracer tracer.ITracer) *redis_database.Database {
	self := redis_database.NewDatabase(config, appConfig, logger, tracer)
	if self == nil {
		return nil
	}

	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			return redis_database.Connect(ctx, self)
		},
		OnStop: func(ctx context.Context) error {
			return redis_database.Disconnect(ctx, self)
		},
	})
	return self
}
