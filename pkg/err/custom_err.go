package err

import (
	"fmt"
	"net/http"

	grpc_codes "google.golang.org/grpc/codes"
	grpc_status "google.golang.org/grpc/status"
)

type CustomErr interface {
	ToGrpcError() error
	ToHttpError() (int, string)
}

type InvalidErr struct {
	Err string
}

func (ie *InvalidErr) Error() string {
	return fmt.Sprintf("INVALID: %s", ie.Err)
}
func (ie *InvalidErr) ToGrpcError() error {
	return grpc_status.Error(grpc_codes.InvalidArgument, ie.Err)
}
func (ie *InvalidErr) ToHttpError() (int, string) {
	return http.StatusBadRequest, ie.Err
}

type DuplicatedErr struct {
	Err string
}

func (de *DuplicatedErr) Error() string {
	return fmt.Sprintf("DUPLICATED: %s", de.Err)
}
func (de *DuplicatedErr) ToGrpcError() error {
	return grpc_status.Error(grpc_codes.AlreadyExists, de.Err)
}
func (de *DuplicatedErr) ToHttpError() (int, string) {
	return http.StatusAlreadyReported, de.Err
}

type NotFoundErr struct {
	Err string
}

func (nfe *NotFoundErr) Error() string {
	return fmt.Sprintf("NOT_FOUND: %s", nfe.Err)
}
func (nfe *NotFoundErr) ToGrpcError() error {
	return grpc_status.Error(grpc_codes.NotFound, nfe.Err)
}
func (nfe *NotFoundErr) ToHttpError() (int, string) {
	return http.StatusNotFound, nfe.Err
}

type FailedPreconditionErr struct {
	Err string
}

func (fpe *FailedPreconditionErr) Error() string {
	return fmt.Sprintf("FAILED_PRECONDITION: %s", fpe.Err)
}
func (fpe *FailedPreconditionErr) ToGrpcError() error {
	return grpc_status.Error(grpc_codes.FailedPrecondition, fpe.Err)
}
func (fpe *FailedPreconditionErr) ToHttpError() (int, string) {
	return http.StatusPreconditionFailed, fpe.Err
}
