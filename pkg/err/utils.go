package err

import (
	"net/http"

	grpccodes "google.golang.org/grpc/codes"
	grpcstatus "google.golang.org/grpc/status"
)

func TranslateToGrpcError(err error) error {
	if customErr, ok := err.(CustomErr); ok {
		return customErr.ToGrpcError()
	}
	return grpcstatus.Error(grpccodes.Internal, err.Error())
}

func TranslateToHttpError(err error) (int, string) {
	if customErr, ok := err.(CustomErr); ok {
		return customErr.ToHttpError()
	}
	return http.StatusInternalServerError, err.Error()
}
