package logger_fx

import (
	"context"

	"go.uber.org/fx"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/app"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
)

var LoggerModule = fx.Provide(NewLoggerForFx)

func NewLoggerForFx(lc fx.Lifecycle, appConfig *app.Config) (logger.ILogger, error) {
	self, err := logger.NewLogger(appConfig)
	if err != nil {
		return nil, err
	}
	if self == nil {
		return nil, nil
	}

	lc.Append(fx.Hook{
		OnStop: func(ctx context.Context) error {
			return logger.Sync(ctx, self)
		},
	})
	return self, nil
}
