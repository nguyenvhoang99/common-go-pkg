package logger

import (
	"context"
	"fmt"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/app"
	zap_logger "gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger/zap"
)

// Logger is exclusive to Zap
func NewLogger(appConfig *app.Config) (ILogger, error) {
	return zap_logger.NewZapLogger(appConfig)
}

func Sync(ctx context.Context, logger ILogger) error {
	zapLogger, ok := logger.(*zap_logger.ZapLogger)
	if !ok {
		return fmt.Errorf("invalid type of logger")
	}
	return zapLogger.Sync(ctx)
}
