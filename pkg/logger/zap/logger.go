package zap_logger

import (
	"context"
	"time"

	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/app"
)

type ZapLogger struct {
	logger *zap.Logger
}

func NewZapLogger(appConfig *app.Config) (*ZapLogger, error) {
	zapLogger, err := initConfig(appConfig).Build(zap.AddCallerSkip(1))
	if err != nil {
		return nil, err
	}
	return &ZapLogger{
		logger: zapLogger,
	}, nil
}

func (zl *ZapLogger) Info(msg string, fields ...zapcore.Field) {
	zl.logger.Info(msg, fields...)
}

func (zl *ZapLogger) InfoCtx(ctx context.Context, msg string, fields ...zapcore.Field) {
	zl.logger.Info(msg, getCtxFields(ctx, fields)...)
}

func (zl *ZapLogger) Warn(msg string, fields ...zapcore.Field) {
	zl.logger.Warn(msg, fields...)
}

func (zl *ZapLogger) WarnCtx(ctx context.Context, msg string, fields ...zapcore.Field) {
	zl.logger.Warn(msg, getCtxFields(ctx, fields)...)
}

func (zl *ZapLogger) Error(msg string, fields ...zapcore.Field) {
	zl.logger.Error(msg, fields...)
}

func (zl *ZapLogger) ErrorCtx(ctx context.Context, msg string, fields ...zapcore.Field) {
	zl.logger.Error(msg, getCtxFields(ctx, fields)...)
}

func (zl *ZapLogger) Debug(msg string, fields ...zapcore.Field) {
	zl.logger.Debug(msg, fields...)
}

func (zl *ZapLogger) DebugCtx(ctx context.Context, msg string, fields ...zapcore.Field) {
	zl.logger.Debug(msg, getCtxFields(ctx, fields)...)
}

func (zl *ZapLogger) Fatal(msg string, fields ...zapcore.Field) {
	zl.logger.Fatal(msg, fields...)
}

func (zl *ZapLogger) FatalCtx(ctx context.Context, msg string, fields ...zapcore.Field) {
	zl.logger.Fatal(msg, getCtxFields(ctx, fields)...)
}

func (zl *ZapLogger) Panic(msg string, fields ...zapcore.Field) {
	zl.logger.Panic(msg, fields...)
}

func (zl *ZapLogger) PanicCtx(ctx context.Context, msg string, fields ...zapcore.Field) {
	zl.logger.Panic(msg, getCtxFields(ctx, fields)...)
}

func (zl *ZapLogger) Sync(ctx context.Context) error {
	// TODO: Check status of issue returning err from Sync() method at https://github.com/uber-go/zap/issues/880
	zl.logger.Info("syncing logs")
	zl.logger.Sync()
	return nil
}

func initConfig(appConfig *app.Config) zap.Config {
	var config zap.Config
	if appConfig.Env == app.ProductionEnv {
		config = zap.NewProductionConfig()
	} else {
		config = zap.NewDevelopmentConfig()
	}
	config.EncoderConfig.EncodeTime = encodeTimeLogger
	return config
}

func encodeTimeLogger(time time.Time, encoder zapcore.PrimitiveArrayEncoder) {
	encoder.AppendString(time.Format("2006-01-02 15:04:05.000000"))
}

func getCtxFields(ctx context.Context, fields []zapcore.Field) []zapcore.Field {
	span := trace.SpanFromContext(ctx)
	return append(fields,
		zap.String("trace_id", span.SpanContext().TraceID().String()),
		zap.String("span_id", span.SpanContext().SpanID().String()))
}
