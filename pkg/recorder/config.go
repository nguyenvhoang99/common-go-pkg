package recorder

import (
	"fmt"

	"github.com/spf13/viper"
)

type Config struct {
	Type string `mapstructure:"type"`
}

func NewConfig() (*Config, error) {
	if !viper.InConfig("recorder") {
		return nil, nil
	}

	config := &Config{}
	if err := viper.UnmarshalKey("recorder", config); err != nil {
		return nil, err
	}

	if config.Type == "" {
		return nil, fmt.Errorf("invalid config for recorder: type=EMPTY")
	}
	return config, nil
}
