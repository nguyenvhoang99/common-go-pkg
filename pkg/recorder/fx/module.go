package recorder_fx

import (
	"go.uber.org/fx"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/recorder"
)

var RecorderModule = fx.Provide(recorder.NewConfig, recorder.NewRecorder)
