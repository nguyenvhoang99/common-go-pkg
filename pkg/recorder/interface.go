package recorder

import (
	"net/http"
	"time"
)

type IRecorder interface {
	Record(service, actionName, actionType string, startTime time.Time, err error)

	ServeHTTP(reswr http.ResponseWriter, req *http.Request)
}
