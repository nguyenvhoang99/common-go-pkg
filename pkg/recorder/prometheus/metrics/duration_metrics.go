package prometheus_metrics

import "github.com/prometheus/client_golang/prometheus"

func NewDurationMetrics(constLabels prometheus.Labels) *prometheus.HistogramVec {
	return prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name:        "duration",
		Help:        "Duration metrics",
		ConstLabels: constLabels,
		Buckets: []float64{
			0.001, 0.002, 0.003, 0.004, 0.005, 0.006, 0.007, 0.008, 0.009, 0.010,
			0.015, 0.020, 0.025, 0.030, 0.035, 0.040, 0.045, 0.050, 0.055, 0.060,
			0.070, 0.080, 0.090, 0.100, 0.110, 0.120, 0.130, 0.140, 0.150, 0.160,
			0.200, 0.220, 0.240, 0.260, 0.280, 0.300, 0.320, 0.340, 0.360, 0.400,
			0.440, 0.480, 0.520, 0.560, 0.600, 0.640, 0.680, 0.720, 0.760, 0.800,
			0.900, 1.000, 1.100, 1.200, 1.300, 1.400, 1.500, 1.600, 1.700, 1.800,
			2.000, 2.200, 2.400, 2.600, 2.800, 3.000, 3.200, 3.400, 3.600, 3.800,
			4.000, 5.000, 6.000, 7.000, 8.000, 9.000, 10.00, 20.00, 40.00, 60.00,
		},
	}, []string{"service", "action_name", "action_type"})
}
