package prometheus_metrics

import "github.com/prometheus/client_golang/prometheus"

func NewErrMetrics(constLabels prometheus.Labels) *prometheus.CounterVec {
	return prometheus.NewCounterVec(prometheus.CounterOpts{
		Name:        "err",
		Help:        "Err metrics",
		ConstLabels: constLabels,
	}, []string{"service", "action_name", "action_type", "err"})
}
