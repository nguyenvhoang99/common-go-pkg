package prometheus_metrics

import (
	"runtime"
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

type SystemMetrics struct {
	numGoroutineDesc *prometheus.Desc
	numCPUDesc       *prometheus.Desc
	upTimeDesc       *prometheus.Desc
	startTime        time.Time
}

func NewSystemMetrics(constLabels prometheus.Labels) *SystemMetrics {
	return &SystemMetrics{
		numCPUDesc:       prometheus.NewDesc("cpu_count", "CPU count", nil, constLabels),
		numGoroutineDesc: prometheus.NewDesc("goroutine_count", "Goroutine count", nil, constLabels),
		upTimeDesc:       prometheus.NewDesc("uptime_in_seconds", "Uptime in seconds", nil, constLabels),
		startTime:        time.Now(),
	}
}

func (m *SystemMetrics) Describe(c chan<- *prometheus.Desc) {
	c <- m.numGoroutineDesc
	c <- m.numCPUDesc
	c <- m.upTimeDesc
}

func (m *SystemMetrics) Collect(c chan<- prometheus.Metric) {
	metric, err := prometheus.NewConstMetric(m.numGoroutineDesc, prometheus.GaugeValue, float64(runtime.NumGoroutine()))
	if err == nil {
		c <- metric
	}
	metric, err = prometheus.NewConstMetric(m.numCPUDesc, prometheus.GaugeValue, float64(runtime.NumCPU()))
	if err == nil {
		c <- metric
	}
	metric, err = prometheus.NewConstMetric(m.upTimeDesc, prometheus.GaugeValue, time.Since(m.startTime).Seconds())
	if err == nil {
		c <- metric
	}
}
