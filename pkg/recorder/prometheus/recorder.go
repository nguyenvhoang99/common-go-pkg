package prometheus_recorder

import (
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/app"
	prometheus_metrics "gitlab.com/nguyenvhoang99/common-go-pkg/pkg/recorder/prometheus/metrics"
)

type PrometheusRecorder struct {
	durationMetrics *prometheus.HistogramVec
	errMetrics      *prometheus.CounterVec
}

func NewPrometheusRecorder(appConfig *app.Config) *PrometheusRecorder {
	constLabels := prometheus.Labels{
		"application": appConfig.GetServiceName(),
	}

	systemMetrics := prometheus_metrics.NewSystemMetrics(constLabels)

	durationMetrics := prometheus_metrics.NewDurationMetrics(constLabels)
	errMetrics := prometheus_metrics.NewErrMetrics(constLabels)

	prometheus.MustRegister(systemMetrics, durationMetrics, errMetrics)

	return &PrometheusRecorder{
		durationMetrics: durationMetrics,
		errMetrics:      errMetrics,
	}
}

func (pr *PrometheusRecorder) Record(service, actionName, actionType string,
	startTime time.Time, err error) {
	duration := time.Since(startTime).Seconds()
	pr.durationMetrics.WithLabelValues(service, actionName, actionType).Observe(duration)

	if err != nil {
		pr.errMetrics.WithLabelValues(service, actionType, actionName, err.Error()).Inc()
	}
}

func (pr *PrometheusRecorder) ServeHTTP(reswr http.ResponseWriter, req *http.Request) {
	promhttp.Handler().ServeHTTP(reswr, req)
}
