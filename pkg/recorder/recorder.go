package recorder

import (
	"fmt"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/app"
	prometheus_recorder "gitlab.com/nguyenvhoang99/common-go-pkg/pkg/recorder/prometheus"
)

const (
	prometheus = "prometheus"
)

func NewRecorder(config *Config, appConfig *app.Config) (IRecorder, error) {
	if config == nil {
		return nil, nil
	}

	var recorder IRecorder
	var err error

	switch config.Type {
	case prometheus:
		recorder = prometheus_recorder.NewPrometheusRecorder(appConfig)
		break
	default:
		err = fmt.Errorf("unimplemented for recorder: type=%s", config.Type)
	}

	if err != nil {
		return nil, err
	}

	return recorder, nil
}
