package grpc_server

import (
	"fmt"

	"github.com/spf13/viper"
)

type Config struct {
	Port                  int               `mapstructure:"port"`
	AuthenticationConfigs map[string]string `mapstructure:"authentication-configs"`
}

func NewConfig() (*Config, error) {
	if !viper.InConfig("grpc-server") {
		return nil, nil
	}

	config := &Config{}
	if err := viper.UnmarshalKey("grpc-server", config); err != nil {
		return nil, err
	}

	if config.Port <= 0 {
		return nil, fmt.Errorf("invalid config for grpc server: port=%d", config.Port)
	}
	return config, nil
}
