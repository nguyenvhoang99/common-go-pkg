package grpc_server_fx

import (
	"context"

	"go.uber.org/fx"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/app"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/recorder"
	grpc_server "gitlab.com/nguyenvhoang99/common-go-pkg/pkg/server/grpc"
)

var ServerConfigModule = fx.Provide(grpc_server.NewConfig)
var ServerModule = fx.Invoke(NewGrpcServerForFx)

func NewGrpcServerForFx(lc fx.Lifecycle, config *grpc_server.Config, appConfig *app.Config,
	services []*grpc_server.Service, recorder recorder.IRecorder, logger logger.ILogger) error {
	self, err := grpc_server.NewServer(config, appConfig, services, logger, recorder)
	if err != nil {
		return err
	}
	if self == nil {
		return nil
	}

	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			return grpc_server.Start(ctx, self)
		},
		OnStop: func(ctx context.Context) error {
			return grpc_server.Stop(ctx, self)
		},
	})
	return nil
}
