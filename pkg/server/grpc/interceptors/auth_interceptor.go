package grpc_interceptors

import (
	"context"
	"strings"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/utils"
)

const (
	ClientIdMetadataKey  = "client-id"
	ClientKeyMetadataKey = "client-key"
)

func NewAuthUnaryServerInterceptor(authenticationConfigs map[string]string, authorizationConfigs map[string][]string) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		requestMetadata, ok := metadata.FromIncomingContext(ctx)
		if !ok {
			return handler(ctx, req)
		}

		metadataClientIds, metadataClientKeys := requestMetadata.Get(ClientIdMetadataKey), requestMetadata.Get(ClientKeyMetadataKey)
		if len(metadataClientIds) <= 0 {
			return nil, status.Error(codes.Unauthenticated, "empty metadata client-id")
		}
		if len(metadataClientKeys) <= 0 {
			return nil, status.Error(codes.Unauthenticated, "empty metadata client-key")
		}

		// Authentication
		if len(authenticationConfigs) > 0 {
			clientKey, ok := authenticationConfigs[metadataClientIds[0]]
			if !ok {
				return nil, status.Error(codes.Unauthenticated, "not found client-id for authenticating")
			}
			if clientKey != metadataClientKeys[0] {
				return nil, status.Error(codes.Unauthenticated, "mismatch client-key for authenticating")
			}
		}

		method := strings.ToLower(info.FullMethod[1:])

		// Authorization
		if len(authorizationConfigs) > 0 {
			clientIds, ok := authorizationConfigs[method]
			if !ok || utils.Contains(clientIds, metadataClientIds[0]) {
				return nil, status.Error(codes.Unauthenticated, "mismatch client-id for authorization")
			}
		}

		return handler(ctx, req)
	}
}
