package grpc_interceptors

import "google.golang.org/grpc/health/grpc_health_v1"

var healthServicePrefix = "/" + grpc_health_v1.Health_ServiceDesc.ServiceName
