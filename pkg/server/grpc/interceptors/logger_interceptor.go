package grpc_interceptors

import (
	"context"
	"strings"

	"go.uber.org/zap"
	"google.golang.org/grpc"
	grpc_codes "google.golang.org/grpc/codes"
	grpc_status "google.golang.org/grpc/status"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
)

func NewLoggerUnaryServerInterceptor(logger logger.ILogger) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		if strings.HasPrefix(info.FullMethod, healthServicePrefix) {
			return handler(ctx, req)
		}

		res, err := handler(ctx, req)
		if err != nil {
			grpcErr, ok := grpc_status.FromError(err)
			if !ok || grpcErr.Code() == grpc_codes.Internal {
				logger.ErrorCtx(ctx, "grpc handler", zap.String("method", info.FullMethod),
					zap.Any("request", req), zap.Error(err))
			} else {
				logger.WarnCtx(ctx, "grpc handler", zap.String("method", info.FullMethod),
					zap.Any("request", req), zap.Any("response", res))
			}
		} else {
			logger.InfoCtx(ctx, "grpc handler",
				zap.Any("request", req), zap.Any("response", res))
		}
		return res, err
	}
}
