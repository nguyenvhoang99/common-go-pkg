package grpc_interceptors

import (
	"context"
	"strings"
	"time"

	"google.golang.org/grpc"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/app"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/recorder"
)

func NewRecorderUnaryServerInterceptor(appConfig *app.Config, recorder recorder.IRecorder) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		if strings.HasPrefix(info.FullMethod, healthServicePrefix) {
			return handler(ctx, req)
		}

		method := strings.ToLower(info.FullMethod[1:])

		var startTime time.Time = time.Now()
		var err error
		defer func() {
			go recorder.Record(appConfig.Name, method, "handler-grpc", startTime, err)
		}()

		res, err := handler(ctx, req)
		return res, err
	}
}
