package grpc_interceptors

import (
	"strings"

	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"google.golang.org/grpc"
)

func NewTracerUnaryServerInterceptor() grpc.UnaryServerInterceptor {
	return otelgrpc.UnaryServerInterceptor(
		otelgrpc.WithInterceptorFilter(func(interceptorInfo *otelgrpc.InterceptorInfo) bool {
			if strings.HasPrefix(interceptorInfo.Method, healthServicePrefix) {
				return false
			}
			return true
		}),
	)
}
