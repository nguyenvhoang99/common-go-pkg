package grpc_server

import (
	"context"
	"fmt"
	"net"

	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health"
	"google.golang.org/grpc/health/grpc_health_v1"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/app"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/recorder"
	grpc_interceptors "gitlab.com/nguyenvhoang99/common-go-pkg/pkg/server/grpc/interceptors"
)

type Server struct {
	server *grpc.Server
	port   int
	logger logger.ILogger
}

type Service struct {
	ServiceDesc         *grpc.ServiceDesc
	ServiceImpl         interface{}
	AuthorizationConfig map[string][]string // key: method, value: clientIds
}

func NewServer(config *Config, appConfig *app.Config, services []*Service,
	logger logger.ILogger, recorder recorder.IRecorder) (*Server, error) {
	if config == nil {
		return nil, nil
	}

	authorizationConfigs := make(map[string][]string)
	for _, service := range services {
		for method, clientIds := range service.AuthorizationConfig {
			if _, ok := authorizationConfigs[method]; !ok {
				authorizationConfigs[method] = make([]string, 0)
			}
			authorizationConfigs[method] = append(authorizationConfigs[method], clientIds...)
		}
	}

	server := grpc.NewServer(
		grpc.ChainUnaryInterceptor(
			grpc_interceptors.NewLoggerUnaryServerInterceptor(logger),
			grpc_interceptors.NewRecorderUnaryServerInterceptor(appConfig, recorder),
			grpc_interceptors.NewTracerUnaryServerInterceptor(),
			grpc_interceptors.NewAuthUnaryServerInterceptor(config.AuthenticationConfigs, authorizationConfigs),
		),
	)

	// business services
	if services != nil && len(services) > 0 {
		for _, service := range services {
			server.RegisterService(service.ServiceDesc, service.ServiceImpl)
		}
	}
	// health service
	grpc_health_v1.RegisterHealthServer(server, health.NewServer())

	return &Server{
		server: server,
		port:   config.Port,
		logger: logger,
	}, nil
}

func Start(ctx context.Context, self *Server) error {
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", self.port))
	if err != nil {
		return err
	}

	self.logger.Info("starting for grpc server", zap.Int("port", self.port))
	go func() {
		if err := self.server.Serve(listener); err != nil && err != grpc.ErrServerStopped {
			self.logger.Fatal("failed to start for grpc server", zap.Int("port", self.port), zap.Error(err))
		}
	}()
	return nil
}

func Stop(ctx context.Context, self *Server) error {
	self.logger.Info("stopping for grpc server")
	self.server.GracefulStop()
	self.logger.Info("stopped for grpc server")
	return nil
}
