package http_server

import (
	"fmt"

	"github.com/spf13/viper"
)

type Config struct {
	Port int `mapstructure:"port"`
}

func NewConfig() (*Config, error) {
	if !viper.InConfig("http-server") {
		return nil, nil
	}

	config := &Config{}
	if err := viper.UnmarshalKey("http-server", config); err != nil {
		return nil, err
	}

	if config.Port <= 0 {
		return nil, fmt.Errorf("invalid config for http server: port=%d", config.Port)
	}
	return config, nil
}
