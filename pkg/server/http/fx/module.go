package http_server_fx

import (
	"context"
	"net/http"

	"go.uber.org/fx"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/app"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/recorder"
	http_server "gitlab.com/nguyenvhoang99/common-go-pkg/pkg/server/http"
)

var ServerConfigModule = fx.Provide(http_server.NewConfig)
var ServerModule = fx.Invoke(NewHttpServerForFx)

func NewHttpServerForFx(lc fx.Lifecycle, config *http_server.Config, appConfig *app.Config,
	router *http.ServeMux, recorder recorder.IRecorder, logger logger.ILogger) error {
	self, err := http_server.NewServer(config, appConfig, router, logger, recorder)
	if err != nil {
		return err
	}
	if self == nil {
		return nil
	}

	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			return http_server.Start(ctx, self)
		},
		OnStop: func(ctx context.Context) error {
			return http_server.Stop(ctx, self)
		},
	})
	return nil
}
