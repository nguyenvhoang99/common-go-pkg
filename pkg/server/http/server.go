package http_server

import (
	"context"
	"fmt"
	"net"
	"net/http"

	"go.uber.org/zap"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/app"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/recorder"
)

type Server struct {
	server *http.Server
	port   int
	logger logger.ILogger
}

func NewServer(config *Config, appConfig *app.Config, router *http.ServeMux,
	logger logger.ILogger, recorder recorder.IRecorder) (*Server, error) {
	if config == nil {
		return nil, nil
	}

	server := &http.Server{
		Addr: fmt.Sprintf(":%d", config.Port),
	}

	var handler *http.ServeMux
	// business services
	if router != nil {
		handler = router
	} else {
		handler = http.NewServeMux()
	}
	// health service
	handler.HandleFunc("/health", func(reswr http.ResponseWriter, req *http.Request) {
		reswr.WriteHeader(200)
	})
	// metrics service
	if recorder != nil {
		handler.HandleFunc("/metrics", func(reswr http.ResponseWriter, req *http.Request) {
			recorder.ServeHTTP(reswr, req)
		})
	}
	server.Handler = handler

	return &Server{
		server: server,
		port:   config.Port,
		logger: logger,
	}, nil
}

func Start(ctx context.Context, self *Server) error {
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", self.port))
	if err != nil {
		return err
	}

	self.logger.Info("starting for http server", zap.Int("port", self.port))
	go func() {
		if err := self.server.Serve(listener); err != nil && err != http.ErrServerClosed {
			self.logger.Fatal("failed to start for http server", zap.Int("port", self.port), zap.Error(err))
		}
	}()
	return nil
}

func Stop(ctx context.Context, self *Server) error {
	self.logger.Info("stopping for http server")
	if err := self.server.Shutdown(ctx); err != nil {
		self.logger.Error("failed to stop http server", zap.Error(err))
		return err
	}
	self.logger.Info("stopped for http server")
	return nil
}
