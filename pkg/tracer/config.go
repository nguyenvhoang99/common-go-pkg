package tracer

import (
	"fmt"
	"log"

	"github.com/spf13/viper"
)

type Config struct {
	Type        string  `mapstructure:"type"`
	SampleRatio float64 `mapstructure:"sample-ratio"`
}

func NewConfig() (*Config, error) {
	if !viper.InConfig("tracer") {
		return nil, nil
	}

	config := &Config{}
	if err := viper.UnmarshalKey("tracer", config); err != nil {
		return nil, err
	}

	if config.Type == "" {
		return nil, fmt.Errorf("invalid config for tracer: type=EMPTY")
	}
	if config.SampleRatio < 0 {
		log.Printf("unexpected config for tracer: sample-ratio=%f", config.SampleRatio)
		config.SampleRatio = 0.0
		log.Printf("fallback to config for tracer: sample-ratio=0.0")
	}
	if config.SampleRatio > 1 {
		log.Printf("unexpected config for tracer: sample-ratio=%f", config.SampleRatio)
		config.SampleRatio = 1.0
		log.Printf("fallback to config for tracer: sample-ratio=1.0")
	}
	return config, nil
}
