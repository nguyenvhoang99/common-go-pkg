package tracer_fx

import (
	"context"

	"go.uber.org/fx"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/app"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/tracer"
)

var TracerModule = fx.Provide(tracer.NewConfig, NewTracerForFx)

func NewTracerForFx(lc fx.Lifecycle, config *tracer.Config, appConfig *app.Config,
	logger logger.ILogger) (tracer.ITracer, error) {
	self, err := tracer.NewTracer(config, appConfig, logger)
	if err != nil {
		return nil, err
	}
	if self == nil {
		return nil, nil
	}

	lc.Append(fx.Hook{
		OnStop: func(ctx context.Context) error {
			return tracer.Flush(ctx, self)
		},
	})
	return self, nil
}
