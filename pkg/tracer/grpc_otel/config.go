package grpc_otel_tracer

import (
	"fmt"

	"github.com/spf13/viper"
)

type Config struct {
	Endpoint string `mapstructure:"endpoint"`
	UseSSL   bool   `mapstructure:"use-ssl"`
}

func NewConfig() (*Config, error) {
	if !viper.InConfig("tracer.grpc-otel") {
		return nil, nil
	}

	config := &Config{}
	if err := viper.UnmarshalKey("tracer.grpc-otel", config); err != nil {
		return nil, err
	}

	if config.Endpoint == "" {
		return nil, fmt.Errorf("invalid config for grpc-otel tracer: endpoint=EMPTY")
	}
	return config, nil
}
