package grpc_otel_tracer

import (
	"context"
	"fmt"

	"go.opentelemetry.io/otel/exporters/otlp/otlptrace"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracegrpc"
)

func NewExporter() (*otlptrace.Exporter, error) {
	config, err := NewConfig()
	if err != nil {
		return nil, err
	}
	if config == nil {
		return nil, fmt.Errorf("invalid config for grpc-otel tracer: nil")
	}

	options := make([]otlptracegrpc.Option, 0)
	options = append(options, otlptracegrpc.WithEndpoint(config.Endpoint))
	if !config.UseSSL {
		options = append(options, otlptracegrpc.WithInsecure())
	}

	spanExporter, err := otlptracegrpc.New(context.Background(), options...)
	if err != nil {
		return nil, err
	}
	return spanExporter, nil
}
