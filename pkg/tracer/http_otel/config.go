package http_otel_tracer

import (
	"fmt"

	"github.com/spf13/viper"
)

type Config struct {
	Endpoint          string `mapstructure:"endpoint"`
	URLPath           string `mapstructure:"url-path"`
	UseSSL            bool   `mapstructure:"use-ssl"`
	UseAuthentication bool   `mapstructure:"use-authentication"`
	Username          string `mapstructure:"username"`
	Password          string `mapstructure:"password"`
}

func NewConfig() (*Config, error) {
	if !viper.InConfig("tracer.http-otel") {
		return nil, nil
	}

	config := &Config{}
	if err := viper.UnmarshalKey("tracer.http-otel", config); err != nil {
		return nil, err
	}

	if config.Endpoint == "" {
		return nil, fmt.Errorf("invalid config for http-otel tracer: endpoint=EMPTY")
	}
	if config.UseAuthentication {
		if config.Username == "" {
			return nil, fmt.Errorf("invalid config for http-otel tracer: use-authentication=true | username=EMPTY")
		}
		if config.Password == "" {
			return nil, fmt.Errorf("invalid config for http-otel tracer: use-authentication=true | password=EMPTY")
		}
	}
	return config, nil
}
