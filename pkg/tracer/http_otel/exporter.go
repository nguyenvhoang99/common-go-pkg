package http_otel_tracer

import (
	"context"
	"crypto/tls"
	"encoding/base64"
	"fmt"

	"go.opentelemetry.io/otel/exporters/otlp/otlptrace"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracehttp"
)

func NewExporter() (*otlptrace.Exporter, error) {
	config, err := NewConfig()
	if err != nil {
		return nil, err
	}
	if config == nil {
		return nil, fmt.Errorf("invalid config for http-otel tracer: nil")
	}

	options := make([]otlptracehttp.Option, 0)
	options = append(options, otlptracehttp.WithEndpoint(config.Endpoint))
	options = append(options, otlptracehttp.WithURLPath(config.URLPath))
	if !config.UseSSL {
		options = append(options, otlptracehttp.WithInsecure())
	} else {
		options = append(options, otlptracehttp.WithTLSClientConfig(&tls.Config{}))
	}
	headers := make(map[string]string)
	if config.UseAuthentication {
		// This is exclusive to Basic Authentication
		headers["Authorization"] = base64.StdEncoding.EncodeToString(
			[]byte(fmt.Sprintf("Basic %s:%s", config.Username, config.Password)),
		)
	}
	options = append(options, otlptracehttp.WithHeaders(headers))

	spanExporter, err := otlptracehttp.New(context.Background(), options...)
	if err != nil {
		return nil, err
	}
	return spanExporter, nil
}
