package tracer

import (
	"context"

	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

type ITracer interface {
	Trace(ctx context.Context, spanName string, spanAttributes ...attribute.KeyValue) (context.Context, trace.Span)
}
