package tracer

import (
	"context"
	"fmt"

	"go.opentelemetry.io/contrib/propagators/b3"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace"
	"go.opentelemetry.io/otel/sdk/resource"
	sdk_trace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.16.0"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"

	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/app"
	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
	grpc_otel_tracer "gitlab.com/nguyenvhoang99/common-go-pkg/pkg/tracer/grpc_otel"
	http_otel_tracer "gitlab.com/nguyenvhoang99/common-go-pkg/pkg/tracer/http_otel"
)

const (
	grpcOtel = "grpc-otel"
	httpOtel = "http-otel"
)

type OtelTracer struct {
	tracerProvider *sdk_trace.TracerProvider
	logger         logger.ILogger
}

// Tracer is exclusive to Otel
func NewTracer(config *Config, appConfig *app.Config, logger logger.ILogger) (ITracer, error) {
	if config == nil {
		return nil, nil
	}

	var spanExporter *otlptrace.Exporter
	var err error

	switch config.Type {
	case grpcOtel:
		spanExporter, err = grpc_otel_tracer.NewExporter()
	case httpOtel:
		spanExporter, err = http_otel_tracer.NewExporter()
	default:
		err = fmt.Errorf("unimplemented for otel tracer: type=%s", config.Type)
	}

	if err != nil {
		return nil, err
	}

	traceProvider := initTracerProvider(config, appConfig, spanExporter)
	initTextMapPropagator()
	return &OtelTracer{
		tracerProvider: traceProvider,
		logger:         logger,
	}, nil
}

func (ot *OtelTracer) Trace(ctx context.Context, spanName string, spanAttributes ...attribute.KeyValue) (context.Context, trace.Span) {
	return ot.tracerProvider.Tracer("").Start(ctx, spanName, trace.WithAttributes(spanAttributes...))
}

func Flush(ctx context.Context, tracer ITracer) error {
	self, ok := tracer.(*OtelTracer)
	if !ok {
		return fmt.Errorf("invalid type of tracer")
	}

	self.logger.Info("flushing traces")
	err := self.tracerProvider.Shutdown(ctx)
	if err != nil {
		self.logger.Error("failed to flush traces", zap.Error(err))
		return err
	}
	self.logger.Info("flushed traces")
	return nil
}

func initTracerProvider(config *Config, appConfig *app.Config, spanExporter *otlptrace.Exporter) *sdk_trace.TracerProvider {
	tracerSampler := sdk_trace.ParentBased(sdk_trace.TraceIDRatioBased(config.SampleRatio))
	tracerResource := resource.NewSchemaless(semconv.ServiceNameKey.String(appConfig.GetServiceName()))

	tracerProvider := sdk_trace.NewTracerProvider(
		sdk_trace.WithSpanProcessor(sdk_trace.NewBatchSpanProcessor(spanExporter)),
		sdk_trace.WithSampler(tracerSampler),
		sdk_trace.WithResource(tracerResource),
	)
	otel.SetTracerProvider(tracerProvider)
	return tracerProvider
}

func initTextMapPropagator() {
	b3Propagator := b3.New(b3.WithInjectEncoding(b3.B3MultipleHeader))
	otel.SetTextMapPropagator(b3Propagator)
}
