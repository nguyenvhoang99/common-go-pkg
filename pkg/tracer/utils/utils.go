package tracer_utils

import (
	"context"

	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

func TraceErr(ctx context.Context, err error) {
	span := trace.SpanFromContext(ctx)
	if span.IsRecording() {
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
	}
}

func NewBgCtxFromCtxWithTrace(ctx context.Context) context.Context {
	span := trace.SpanFromContext(ctx)
	if span.IsRecording() {
		remoteSpanContext := trace.NewSpanContext(trace.SpanContextConfig{
			TraceID:    span.SpanContext().TraceID(),
			SpanID:     span.SpanContext().SpanID(),
			TraceFlags: 01,
			Remote:     true,
		})
		return trace.ContextWithRemoteSpanContext(context.Background(), remoteSpanContext)
	}
	return context.Background()
}
