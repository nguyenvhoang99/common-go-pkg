package utils

func Contains[T comparable](elements []T, value T) bool {
	for _, element := range elements {
		if value == element {
			return true
		}
	}
	return false
}
